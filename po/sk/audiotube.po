# translation of audiotube.po Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: audiotube\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-07 00:49+0000\n"
"PO-Revision-Date: 2022-04-07 18:01+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: asyncytmusic.cpp:65
#, kde-format
msgid ""
"Running with untested version of ytmusicapi %1. If you experience errors, "
"please report them to your distribution."
msgstr ""

#: contents/ui/AlbumPage.qml:75 contents/ui/LibraryPage.qml:37
#: contents/ui/LibraryPage.qml:63 contents/ui/LibraryPage.qml:84
#: contents/ui/LibraryPage.qml:222 contents/ui/LibraryPage.qml:248
#: contents/ui/LibraryPage.qml:269 contents/ui/LocalPlaylistPage.qml:70
#: contents/ui/LocalPlaylistPage.qml:92 contents/ui/PlaybackHistory.qml:67
#: contents/ui/PlaybackHistory.qml:89 contents/ui/PlaylistPage.qml:69
#: contents/ui/PlaylistPage.qml:84
#, kde-format
msgid "Play"
msgstr "Prehrať"

#: contents/ui/AlbumPage.qml:82 contents/ui/ArtistPage.qml:87
#: contents/ui/LibraryPage.qml:44 contents/ui/LibraryPage.qml:69
#: contents/ui/LibraryPage.qml:92 contents/ui/LibraryPage.qml:229
#: contents/ui/LibraryPage.qml:254 contents/ui/LibraryPage.qml:278
#: contents/ui/LocalPlaylistPage.qml:65 contents/ui/LocalPlaylistPage.qml:99
#: contents/ui/PlaybackHistory.qml:55 contents/ui/PlaybackHistory.qml:102
#: contents/ui/PlaylistPage.qml:62 contents/ui/PlaylistPage.qml:91
#, kde-format
msgid "Shuffle"
msgstr "Zamiešať"

#: contents/ui/AlbumPage.qml:90 contents/ui/LibraryPage.qml:74
#: contents/ui/LibraryPage.qml:100 contents/ui/LibraryPage.qml:259
#: contents/ui/LibraryPage.qml:286 contents/ui/LocalPlaylistPage.qml:105
#: contents/ui/PlaybackHistory.qml:114 contents/ui/PlaylistPage.qml:99
#, kde-format
msgid "Append to queue"
msgstr ""

#: contents/ui/AlbumPage.qml:95 contents/ui/ArtistPage.qml:95
#: contents/ui/PlaylistPage.qml:104
#, kde-format
msgid "Open in Browser"
msgstr ""

#: contents/ui/AlbumPage.qml:100 contents/ui/ArtistPage.qml:100
#: contents/ui/PlaylistPage.qml:109
#, kde-format
msgid "Share"
msgstr ""

#: contents/ui/AlbumPage.qml:110
#, kde-format
msgid "Album • %1"
msgstr ""

#: contents/ui/AlbumPage.qml:162 contents/ui/ArtistPage.qml:199
#: contents/ui/LocalPlaylistPage.qml:170 contents/ui/PlaybackHistory.qml:194
#: contents/ui/PlaylistPage.qml:179 contents/ui/SearchPage.qml:144
#, kde-format
msgid "More"
msgstr ""

#: contents/ui/ArtistPage.qml:81
#, kde-format
msgid "Radio"
msgstr "Rádio"

#: contents/ui/ArtistPage.qml:109
#, fuzzy, kde-format
#| msgid "Artists"
msgid "Artist"
msgstr "Interpreti"

#: contents/ui/ArtistPage.qml:131 contents/ui/SearchPage.qml:72
#, kde-format
msgid ""
"Video playback is not supported yet. Do you want to play only the audio of "
"\"%1\"?"
msgstr ""

#: contents/ui/ArtistPage.qml:148 contents/ui/SearchPage.qml:26
#, kde-format
msgid "Albums"
msgstr "Albumy"

#: contents/ui/ArtistPage.qml:150
#, kde-format
msgid "Singles"
msgstr "Samotné"

#: contents/ui/ArtistPage.qml:152 contents/ui/SearchPage.qml:32
#, kde-format
msgid "Songs"
msgstr ""

#: contents/ui/ArtistPage.qml:154 contents/ui/SearchPage.qml:34
#, kde-format
msgid "Videos"
msgstr "Videá"

#: contents/ui/ConfirmationMessage.qml:16
#, kde-format
msgid "OK"
msgstr "OK"

#: contents/ui/ConfirmationMessage.qml:21
#, kde-format
msgid "Cancel"
msgstr "Zrušiť"

#: contents/ui/dialogs/AddPlaylistDialog.qml:17
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Add playlist"
msgstr "Pridať do zoznamu skladieb"

#: contents/ui/dialogs/AddPlaylistDialog.qml:23
#: contents/ui/dialogs/RenamePlaylistDialog.qml:24
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Playlist Title"
msgstr "Zoznamy skladieb"

#: contents/ui/dialogs/AddPlaylistDialog.qml:28
#: contents/ui/dialogs/RenamePlaylistDialog.qml:30
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Playlist Description"
msgstr "Zoznamy skladieb"

#: contents/ui/dialogs/PlaylistDialog.qml:20
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Add Song to Playlist"
msgstr "Pridať do zoznamu skladieb"

#: contents/ui/dialogs/PlaylistDialog.qml:29
#: contents/ui/LocalPlaylistsPage.qml:90
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "New Playlist"
msgstr "Zoznamy skladieb"

#: contents/ui/dialogs/RenamePlaylistDialog.qml:18
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Rename playlist"
msgstr "Zoznamy skladieb"

#: contents/ui/LibraryPage.qml:30 contents/ui/LibraryPage.qml:123
#: contents/ui/NavigationBar.qml:48 contents/ui/NavigationBar.qml:54
#: contents/ui/Sidebar.qml:131 contents/ui/Sidebar.qml:138
#, kde-format
msgid "Favourites"
msgstr ""

#: contents/ui/LibraryPage.qml:119 contents/ui/LibraryPage.qml:306
#: contents/ui/LibraryPage.qml:403
#, kde-format
msgid "Show All"
msgstr ""

#: contents/ui/LibraryPage.qml:147
#, kde-format
msgid "No Favourites Yet"
msgstr ""

#: contents/ui/LibraryPage.qml:215
#, kde-format
msgid "Most played"
msgstr ""

#: contents/ui/LibraryPage.qml:310 contents/ui/NavigationBar.qml:61
#: contents/ui/NavigationBar.qml:67 contents/ui/Sidebar.qml:150
#: contents/ui/Sidebar.qml:157
#, kde-format
msgid "Played Songs"
msgstr ""

#: contents/ui/LibraryPage.qml:334
#, kde-format
msgid "No Songs Played Yet"
msgstr ""

#: contents/ui/LibraryPage.qml:391 contents/ui/LocalPlaylistsPage.qml:18
#: contents/ui/LocalPlaylistsPage.qml:78 contents/ui/NavigationBar.qml:75
#: contents/ui/SearchPage.qml:30 contents/ui/Sidebar.qml:167
#, kde-format
msgid "Playlists"
msgstr "Zoznamy skladieb"

#: contents/ui/LibraryPage.qml:430
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "No Playlists Yet"
msgstr "Zoznamy skladieb"

#: contents/ui/LibraryPage.qml:450 contents/ui/LibraryPage.qml:472
#: contents/ui/LocalPlaylistsPage.qml:33 contents/ui/LocalPlaylistsPage.qml:55
#, kde-format
msgid "Rename"
msgstr ""

#: contents/ui/LibraryPage.qml:459 contents/ui/LibraryPage.qml:480
#: contents/ui/LocalPlaylistsPage.qml:42 contents/ui/LocalPlaylistsPage.qml:63
#, kde-format
msgid "Delete"
msgstr ""

#: contents/ui/LocalPlaylistPage.qml:21
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Remove from Playlist"
msgstr "Zoznamy skladieb"

#: contents/ui/LocalPlaylistPage.qml:79
#, kde-format
msgid "This playlist is still empty"
msgstr ""

#: contents/ui/main.qml:110 main.cpp:61
#, kde-format
msgid "AudioTube"
msgstr "AudioTube"

#: contents/ui/main.qml:260
#, fuzzy, kde-format
#| msgid "No media playing"
msgid "No song playing"
msgstr "Neprehráva sa žiadne médium"

#: contents/ui/MaximizedPlayerPage.qml:99
#, kde-format
msgid "Close Maximized Player"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:203
#: contents/ui/MinimizedPlayerControls.qml:94
#, kde-format
msgid "No media playing"
msgstr "Neprehráva sa žiadne médium"

#: contents/ui/MaximizedPlayerPage.qml:426
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Remove from Favourites"
msgstr "Zoznamy skladieb"

#: contents/ui/MaximizedPlayerPage.qml:426
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Add to Favourites"
msgstr "Pridať do zoznamu skladieb"

#: contents/ui/MaximizedPlayerPage.qml:451
#, kde-format
msgid "Open Volume Drawer"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:559
#, kde-format
msgid "Unmute Audio"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:559
#, kde-format
msgid "Mute Audio"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:602
#, kde-format
msgid "%1%"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:625
#, kde-format
msgid "Hide Lyrics"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:625
#, kde-format
msgid "Show Lyrics"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:645 contents/ui/SongMenu.qml:183
#: contents/ui/SongMenu.qml:269
#, kde-format
msgid "Share Song"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:691
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Add to a local playlist"
msgstr "Pridať do zoznamu skladieb"

#: contents/ui/MaximizedPlayerPage.qml:728
#, kde-format
msgid "Hide Queue"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:728
#, kde-format
msgid "Show Queue"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:948
#: contents/ui/MaximizedPlayerPage.qml:1190
#, kde-format
msgid "Remove Track"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:987
#, fuzzy, kde-format
#| msgid "Clear"
msgid "Clear Queue"
msgstr "Vyčistiť"

#: contents/ui/MaximizedPlayerPage.qml:1012
#, fuzzy, kde-format
#| msgid "Shuffle"
msgid "Shuffle Queue"
msgstr "Zamiešať"

#: contents/ui/MaximizedPlayerPage.qml:1042
#, kde-format
msgid "Upcoming Songs"
msgstr ""

#: contents/ui/NavigationBar.qml:24 contents/ui/Sidebar.qml:100
#, kde-format
msgid "Library"
msgstr ""

#: contents/ui/NavigationBar.qml:36 contents/ui/Sidebar.qml:116
#, kde-format
msgid "Search"
msgstr "Hľadať"

#: contents/ui/PlaybackHistory.qml:17
#, kde-format
msgid "Unknown list of songs"
msgstr ""

#: contents/ui/PlaybackHistory.qml:135
#, kde-format
msgid "No songs here yet"
msgstr ""

#: contents/ui/PlaylistPage.qml:120
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Playlist"
msgstr "Zoznamy skladieb"

#: contents/ui/SearchHistoryPage.qml:86 contents/ui/SearchWithDropdown.qml:397
#, kde-format
msgid "remove from search history"
msgstr ""

#: contents/ui/SearchPage.qml:16
#, kde-format
msgid "Previous Searches:"
msgstr ""

#: contents/ui/SearchPage.qml:28
#, kde-format
msgid "Artists"
msgstr "Interpreti"

#: contents/ui/SearchPage.qml:36
#, kde-format
msgid "Top Results"
msgstr ""

#: contents/ui/SearchPage.qml:39
#, kde-format
msgid "Unknown"
msgstr "Neznáme"

#: contents/ui/SearchWithDropdown.qml:343
#, kde-format
msgid "No matching previous searches"
msgstr ""

#: contents/ui/ShareMenu.qml:39
#, kde-format
msgid "Share to"
msgstr ""

#: contents/ui/ShareMenu.qml:49 contents/ui/ShareMenu.qml:99
#, kde-format
msgid "Copy Link"
msgstr ""

#: contents/ui/ShareMenu.qml:54 contents/ui/ShareMenu.qml:104
#, kde-format
msgid "Link copied to clipboard"
msgstr ""

#: contents/ui/Sidebar.qml:189
#, kde-format
msgid "About"
msgstr ""

#: contents/ui/Sidebar.qml:203
#, kde-format
msgid "Collapse Sidebar"
msgstr ""

#: contents/ui/SongMenu.qml:107 contents/ui/SongMenu.qml:205
#, fuzzy, kde-format
#| msgid "Play"
msgid "Play Next"
msgstr "Prehrať"

#: contents/ui/SongMenu.qml:116 contents/ui/SongMenu.qml:211
#, kde-format
msgid "Add to queue"
msgstr ""

#: contents/ui/SongMenu.qml:126 contents/ui/SongMenu.qml:221
#, kde-format
msgid "Remove Favourite"
msgstr ""

#: contents/ui/SongMenu.qml:126 contents/ui/SongMenu.qml:221
#, kde-format
msgid "Add Favourite"
msgstr ""

#: contents/ui/SongMenu.qml:143 contents/ui/SongMenu.qml:235
#, kde-format
msgid "Remove from History"
msgstr ""

#: contents/ui/SongMenu.qml:156 contents/ui/SongMenu.qml:244
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Add to playlist"
msgstr "Pridať do zoznamu skladieb"

#: main.cpp:63
#, kde-format
msgid "YouTube Music Player"
msgstr ""

#: main.cpp:65
#, kde-format
msgid "© 2021-2023 Jonah Brüchert, 2021-2023 KDE Community"
msgstr ""

#: main.cpp:66
#, kde-format
msgid "Jonah Brüchert"
msgstr ""

#: main.cpp:66
#, kde-format
msgid "Maintainer"
msgstr ""

#: main.cpp:67
#, kde-format
msgid "Mathis Brüchert"
msgstr ""

#: main.cpp:67
#, kde-format
msgid "Designer"
msgstr ""

#: main.cpp:68
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Roman Paholík"

#: main.cpp:68
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wizzardsk@gmail.com"

#: main.cpp:73
#, kde-format
msgid "Unofficial API for YouTube Music"
msgstr ""

#, fuzzy
#~| msgid "Playlists"
#~ msgid "Clear Playlist"
#~ msgstr "Zoznamy skladieb"

#, fuzzy
#~| msgid "No media playing"
#~ msgid "Now Playing"
#~ msgstr "Neprehráva sa žiadne médium"

#~ msgid "Pause"
#~ msgstr "Pozastaviť"

#~ msgid "Expand"
#~ msgstr "Rozvinúť"
